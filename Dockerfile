FROM centos:7
RUN yum install openssl-devel libffi-devel bzip2-devel -y
RUN yum install wget -y
RUN wget https://www.python.org/ftp/python/3.10.0/Python-3.10.0.tgz
RUN tar xvf Python-3.10.0.tgz
RUN cd Python-3.10.0
RUN yum install python3-pip -y
RUN pip3 install flask
RUN pip3 install flask-jsonpify
RUN pip3 install flask-restful
RUN mkdir python_api
COPY python-api.py /python_api
WORKDIR "/python_api"
CMD ["python3", "python-api.py"]
EXPOSE 5290
